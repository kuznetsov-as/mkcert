# Запуск центра сертификации для SSL в локальной сети

## Установка Mkcert

Устанавливаем на сервере mkcert по инструкции взятой из их [репозитория](https://github.com/FiloSottile/mkcert)
``` bash
sudo apt install libnss3-tools
curl -JLO "https://dl.filippo.io/mkcert/latest?for=linux/amd64"
chmod +x mkcert-v*-linux-amd64
sudo cp mkcert-v*-linux-amd64 /usr/local/bin/mkcert
```

Теперь добавим mkcert в список локальных корневых центров сертификации:
``` bash
mkcert -install
```

Теперь создаем/переходим в папку, где будем хранить сертификат и создаем сертификат для localhost
``` bash
mkcert localhost 127.0.0.1 ::1
```

И/или для своего домена (с локальными тоже работает, но нужен днс сервер где будут соответствующие записи)
``` bash
mkcert dummy.home
```

Эти команды создадут в текущей папке файлы *.pem

Теперь осталось перенести файлы и указать их в настройках веб-сервера (его наличие обязательно). Я выбрал nginx, для примера приведу свой docker-compose.yml:
``` bash
version: "3.8"

services:

  wg-client:
    build: .
    container_name: wg-client
    restart: unless-stopped
    tty: true
    cap_add:
      - NET_ADMIN
      - SYS_MODULE
    sysctls:
      - net.ipv4.conf.all.src_valid_mark=1
    volumes:
      - ./wgconf:/etc/wireguard

  dummy-page:
    image: antonkuznetsov/dummy-page:latest
    container_name: dummy-page
    restart: unless-stopped
    volumes:
      - type: bind
        source: ./WhoAmI/WhoAmI.txt
        target: /home/tony/helloPage/WhoAmI/WhoAmI.txt
    depends_on:
      - "wg-client"
    network_mode: service:wg-client

  nginx:
    image: nginx
    container_name: nginx-dummypage
    restart: unless-stopped
    volumes:
      - ./nginxconf:/etc/nginx
    depends_on:
      - "wg-client"
    network_mode: service:wg-client
```

В нем все просто, есть контейнер делающий из всего этого добра клиента wireguard (wg-client), есть контейнер-одностраничник, который выводит на порт 9090 html с текстом, который указан в файле WhoAmI.txt (dummy-page), ну и конечно сам nginx (nginx-dummypage), который слушает запросы на 80 порт и проксирует их на 9090. Также nginx заботливо подставит наши сертификаты, для этого в папке ./nginxconf нужно разместить конфиг nginx (nginx.conf):


(Подразумевается, что сертификаты создавались для дальнейшей возможности ходить на dummy-page через https://dummy.home, т.е. изначально сертификаты были созданы в папке ./nginxconf/certs при помощи команды `mkcert dummy.home`)

``` bash
events {}
http {
    server {
        listen 443 ssl;
        listen 80;
        server_name dummy.home;
        ssl_protocols TLSv1.2 TLSv1.3;
        ssl_certificate /etc/nginx/certs/dummy.home.pem ;
        ssl_certificate_key /etc/nginx/certs/dummy.home-key.pem;
        location / {
            proxy_pass http://127.0.0.1:9090/;
        }
    }
}
```

## Сохранение корневого сертификата для клиентов

Все готово, теперь необходимо скопировать себе корневой сертификат своего локального центра сертификации (rootCA.pem) и подложить его всем будущим клиентам (Всем, кому будет нужен доступ через https). Сначала нужно его найти, для этого на серваке (где устанавливали mkcert) выполняем слудующую команду:
``` bash
mkcert -CAROOT
```

Получаем в ответе путь, по которому лежит наш корневой сертификат (rootCA.pem)
Любым удобным способом сохраняем его себе и копируем на клиенты.

## Установка корневого сертификата в Windows
Любым удобным способом перемещаем rootCA.pem на винду, после этого есть два варианта

1) Через CMD:
Заходим в cmd из под админа и создаем там переменную $CAROOT, в которую помещаем путь до нашего rootCA.pem
``` bash
set CAROOT=D:\files
```
Устанавливаем mkcert
``` bash
choco install mkcert
```
Стартуем
``` bash
mkcert -install
```

2) Через гуи: 
через пуск ищем **Свойства браузера** --> **Содержание** --> **Сертификаты**  --> **Доверенные корневые центры сертификации** --> **Импорт**

Подставляем туда наш rootCA.pem и дальше там все очевидно, со всем соглашаемся и можем проверять.

## Установка корневого сертификата IOS
Любым удобным способом перемещаем rootCA.pem на iphone, и в проводнике дважды тыкаем на него, после этого в настройках появляются нужные нам поля.
[Вот тут](https://github-com.translate.goog/FiloSottile/mkcert/issues/233?_x_tr_sl=auto&_x_tr_tl=ru&_x_tr_hl=ru#issuecomment-690110809) довольно понятно описаны следующие шаги. (главное досмотреть до конца скрины, тогда все точно будет понятно)

## Заставляем Firefox использовать системные центры сертификации
Для этого в адресной строке браузера пишем `about:config` и получаем доступ к конфигам браузера, затем ищем там 
``` bash
security.enterprise_roots.enabled
```
Переводим это параметр в true и перезапускаем браузер, все готово - проверяем.